function Tournament(teamsPerMatch, numberOfTeams){

  this.teamsPerMatch = teamsPerMatch;
  this.numberOfTeams = numberOfTeams;
  this.numberOfRounds = Helpers.logWithBase(this.teamsPerMatch,this.numberOfTeams);
  this.rounds = [];
  this.teams=[];
  this.getWinnerTeamId = function () {
      return tournamentWinnerTeamId;
  };
  var tournamentWinnerTeamId;

  var getTournamentInfo = function (teamsPerMatch, numberOfTeams) {
    return new Promise(function(resolve, reject){
      Helpers.ajax({
        method : "POST",
        url : "/tournament",
        data : {
          numberOfTeams: numberOfTeams,
          teamsPerMatch: teamsPerMatch
        },
        success: function(data){
          resolve(data);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };
  var announceWinner = function (winnerTeam) {
    document.getElementById('winner').innerHTML = winnerTeam.teamName;
  };
  var getTeamById = function (id, teams) {
    for(var i=0,length=teams.length;i<length;i++){
      if(teams[i].teamId == id){
        return teams[i];
      }
    }
    throw "Cannot find teams with ID given";
  };

  var matchEndCallBack = (function (match, winnerTeamId) {
    var winnerTeam = getTeamById(winnerTeamId, this.teams);
    console.log('The winner of the match ' + match.matchId +' is: ', winnerTeam);
    if(typeof this.rounds[match.roundId+1] != "undefined"){
      var nextRound = this.rounds[match.roundId+1],
          nextMatchId = Math.floor(match.matchId/this.teamsPerMatch),
          nextMatch = nextRound.matches[nextMatchId];
      console.log(nextRound,nextMatchId,nextMatch);

      addTeamToMatch(winnerTeam, nextMatch)
    }
    else{
      console.log('Team ' + winnerTeam.teamName + " won the tournament!");
      tournamentWinnerTeamId = winnerTeam.teamId;
      announceWinner(winnerTeam);
    }
  }).bind(this);

  var addTeamToMatch = function (team, match) {
    match.addTeamToMatch(team);
  };

  var initRounds = (function () {
    var numberOfTeamsInNextRound = this.numberOfTeams;

    // var firstRoundMatchups = [];
    //TODO IN ONE ITERATION
    // for(var i =0,length=matchUps.length;i<length;i++){
    //   // firstRoundMatchups[i] = new Match(0, matchUps[i].match, matchUps[i].teamIds);
    //   firstRoundMatchups[i] = new Match(0, matchUps[i].match, []);
    // }
    // this.rounds.push(new Round(0,firstRoundMatchups));

    for(var i=0; i<this.numberOfRounds; i++){
      var currMatchUps = [];

      for(var j=0, numberOfTeamsInNextRound = numberOfTeamsInNextRound/this.teamsPerMatch;j<numberOfTeamsInNextRound;j++){
        //this.rounds[i].matches[this.rounds[i].matches.length-1].matchId + 1 + j
        currMatchUps[j] = new Match(this.tournamentId, i, j, [], this.teamsPerMatch, matchEndCallBack);
      }

      this.rounds.push(new Round(i, currMatchUps));
    }

    console.log(this.rounds);
  }).bind(this);

  var initTeams = (function (matchUps) {
    for(var i=0,length = matchUps.length;i<length;i++){
      for(var j=0,length2=matchUps[i].teamIds.length;j<length2;j++){
        var newTeam = new Team(matchUps[i].teamIds[j]);
        this.teams.push(newTeam);
        (function (i,newTeam,matchUps,addTeamToMatch) {
          newTeam.getTeamInfo(this.tournamentId).then(
            function (response) {
              addTeamToMatch(newTeam, this.rounds[0].getMatchById(matchUps[i].match));
            }.bind(this),
            function (error) {
              console.log(error);
            }
          );
        }).bind(this)(i, newTeam, matchUps, addTeamToMatch);
      }
    }
  }).bind(this);


  var startTournament = (function (tournamentInfo) {
    this.tournamentId = tournamentInfo.tournamentId;

    //Create Rounds
    initRounds(tournamentInfo.matchUps);
    initTeams(tournamentInfo.matchUps);



    // for(var i=0,length=this.rounds[0].matches.length;i<length;i++){
    //
    //   //Get info about teams
    //   for(var k=0;k<this.teamsPerMatch;k++){
    //     var teamId = this.rounds[0].matches[i].teams[k];
    //     this.teams[teamId] = new Team(teamId);
    //     (function(i,teamId){
    //       var match = this.rounds[0].matches[i];
    //       this.teams[teamId].getTeamInfo(this.tournamentId).then(
    //         function (response) {
    //           this.teams[teamId].teamScore = response.score;
    //           this.teams[teamId].teamName = response.name;
    //           this.rounds[0].matches[i].addTeamToMatch(teamId);
    //
    //           if(match.checkIfMatchCanStart(this.teamsPerMatch)){
    //             match.getWinnerTeamId(this.tournamentId, getTeamScoresByMatch(match)).then(
    //               function (response) {
    //                 console.log('winner is: ' + response);
    //               },
    //               function (error) {
    //
    //               }
    //             );
    //           }
    //         }.bind(this),
    //         function(error){
    //           console.log(error);
    //         }
    //       );
    //     }).bind(this)(i,teamId);
    //   }
    //
    //   //Get match scores
    //   (function(i) {
    //     this.rounds[0].matches[i].getMatchScore(this.tournamentId).then(
    //       function (response) {
    //         var match = this.rounds[0].matches[i];
    //         // console.log("matchcanstart", match.checkIfMatchCanStart());
    //         if(match.checkIfMatchCanStart(this.teamsPerMatch)){
    //           match.getWinnerTeamId(this.tournamentId, getTeamScoresByMatch(match)).then(
    //             function (response) {
    //               console.log('winner is: ' + response);
    //             },
    //             function (error) {
    //
    //             }
    //           );
    //         }
    //       }.bind(this),
    //       function (error) {
    //         console.log(error);
    //       }
    //     );
    //   }).bind(this)(i);
    //   // console.log(this.rounds[0].matches[i]);
    // }
    //Start getting info about teams
    //Start getting info about matches

    //If I have scores of teams participating in Match, get winner, and write it to the next round
  }).bind(this);
  var endTournament = function (winnerTeam) {

  };
  getTournamentInfo(teamsPerMatch, numberOfTeams).then(
    function (response) {
      response = JSON.parse(response);
      startTournament(response);
    },
    function (error) {
      console.log(error);
    }
  );


}