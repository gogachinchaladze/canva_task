var Helpers = (function Helpers(){
  return {
    logWithBase : function(b,n){
      var exp = 0;

      while(true){
        n = n / b;
        exp += 1;

        if(n % 1 != 0){
          return -1;
        }

        if(n == 1) {
          return exp;
        }

      }
    },
    randomColor : function() {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    },
    isTouch : function () {
      return !!('ontouchstart' in window) || window.navigator.msMaxTouchPoints > 0;
    },
    ajax : function (options) {
      if(!options){
        throw "ajaxCall function has no argument - options!";
      }
      else if(!options.url || options.url == ""){
        throw "ajax options has no url specified";
      }
      var customOptions = {
        async : options.async || true,
        data : options.data || {},
        url : options.url,
        method : options.method || 'POST',
        contentType : options.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
        username : options.username || '',
        password : options.password || '',
        error : options.error || function(xhr){console.log(xhr)},
        success : options.success || function(xhr){console.log(xhr)}
      };

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4){
          if(xhttp.status == 200){
            customOptions.success(xhttp.response);
          }
          else{
            customOptions.error(xhttp.response);
          }
        }
      };
      xhttp.open(customOptions.method, customOptions.url, customOptions.async);
      xhttp.setRequestHeader("Content-type", customOptions.contentType);
      (customOptions.method == "GET") ? xhttp.send() : xhttp.send(this.serialize(customOptions.data));
    },
    serialize : function(obj) {
      var str = [];
      for(var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    },
    objToQuery : function (key, arr) {
      var strToReturn = "";
      var counter = 0;
      for(var i in arr){
        if(counter!=0){
          strToReturn += "&";
        }
        strToReturn+= key + "=" + arr[i];
        counter++;
      }
      return strToReturn;
    }
  };
})();
function Match(tournamentId, roundId, matchId, teams, teamsPerMatch, cb){
  this.tournamentId = tournamentId;
  this.roundId = roundId;
  this.matchId = matchId;
  this.teams = teams;
  this.teamsPerMatch = teamsPerMatch;

  var matchScore, winnerTeamId;

  var getMatchScore = function (tournamentId, roundId, matchId) {
    return new Promise(function (resolve, reject) {
      Helpers.ajax({
        method : "GET",
        url : "/match?tournamentId=" +tournamentId+"&round="+roundId+"&match="+matchId,
        success: function(data){
          data = JSON.parse(data);
          matchScore = data.score;
          resolve(matchScore);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };
  var getWinnerScore = function (tournamentId, teamScores, matchScore) {
    return new Promise(function (resolve, reject) {
      Helpers.ajax({
        method : "GET",
        url : "/winner?tournamentId=" + tournamentId + "&" + Helpers.objToQuery("teamScores",teamScores)+"&matchScore="+matchScore,
        success: function(data){
          data = JSON.parse(data);
          resolve(data.score);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };

  var getTeamScoresArr = function (teams) {
    var teamScoresArr = [];
    console.log(teams);
    for(var i =0, length=teams.length; i<length;i++){
      teamScoresArr.push(teams[i].teamScore);
    }

    return teamScoresArr;
  };

  var startMatch = (function () {
    getWinnerScore(this.tournamentId, getTeamScoresArr(this.teams), this.matchId).then(
      function (response) {
        var winnerScore = response;
        for(var i = 0, length = this.teams.length; i<length; i++){
          if(winnerScore == this.teams[i].teamScore){
            winnerTeamId=this.teams[i].teamId;
            endMatch();
            break;
          }
        }
      }.bind(this),
      function (error) {
        console.log(error);
      }
    );
  }).bind(this);
  var endMatch = (function () {
    if(typeof winnerTeamId == "undefined") throw "Winner is not defined yet!";
    cb(this, winnerTeamId);
  }).bind(this);
  var checkIfMatchCanStart = (function () {
    if(this.teams.length == this.teamsPerMatch && typeof matchScore != "undefined"){
      startMatch();
    }
  }).bind(this);

  this.addTeamToMatch = function (team) {
    //TODO CHECK IF ARRAY IS ALREADY FULL
    this.teams.push(team);
    checkIfMatchCanStart();
  };
  this.getWinnerTeamId = function () {
    return winnerTeamId;
  };
  getMatchScore(this.tournamentId,this.roundId,this.matchId).then(
    function (response) {
      matchScore = response;
      checkIfMatchCanStart();
    },
    function (error) {
      console.log(error);
    }
  );

}
function Round(roundId, matchUps){
  this.roundId = roundId;
  this.matches = matchUps;


  this.getMatchById = function (matchId) {
    for(var i=0,length = this.matches.length;i<length;i++){
      if(this.matches[i].matchId == matchId){
        return this.matches[i];
      }
    }
    throw "No matches found with id given";
  }
}
function Team(teamId){
  this.teamId = teamId;

  var setTeamInfo = (function (teamInfo) {
    this.teamScore = teamInfo.score;
    this.teamName = teamInfo.name;
  }).bind(this);

  var getTeamInfoFromServer = function (tournamentId, teamId, resolve, reject) {
    Helpers.ajax({
      method : "GET",
      url : "/team?tournamentId=" +tournamentId + "&teamId=" + teamId,
      success: function(data){
        var teamInfo = JSON.parse(data);
        setTeamInfo(teamInfo);
        resolve();
      },
      error : function (err) {
        reject(error);
      }
    });
  };

  this.getTeamInfo = function (tournamentId) {
    var teamId = this.teamId;
    return new Promise(function (resolve, reject) {
      if(this.teamScore){
        resolve();
      }
      else{
        getTeamInfoFromServer(tournamentId, teamId, resolve, reject);
      }
    });
  };
}
function Tournament(teamsPerMatch, numberOfTeams){

  this.teamsPerMatch = teamsPerMatch;
  this.numberOfTeams = numberOfTeams;
  this.numberOfRounds = Helpers.logWithBase(this.teamsPerMatch,this.numberOfTeams);
  this.rounds = [];
  this.teams=[];
  this.getWinnerTeamId = function () {
      return tournamentWinnerTeamId;
  };
  var tournamentWinnerTeamId;

  var getTournamentInfo = function (teamsPerMatch, numberOfTeams) {
    return new Promise(function(resolve, reject){
      Helpers.ajax({
        method : "POST",
        url : "/tournament",
        data : {
          numberOfTeams: numberOfTeams,
          teamsPerMatch: teamsPerMatch
        },
        success: function(data){
          resolve(data);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };
  var announceWinner = function (winnerTeam) {
    document.getElementById('winner').innerHTML = winnerTeam.teamName;
  };
  var getTeamById = function (id, teams) {
    for(var i=0,length=teams.length;i<length;i++){
      if(teams[i].teamId == id){
        return teams[i];
      }
    }
    throw "Cannot find teams with ID given";
  };

  var matchEndCallBack = (function (match, winnerTeamId) {
    var winnerTeam = getTeamById(winnerTeamId, this.teams);
    console.log('The winner of the match ' + match.matchId +' is: ', winnerTeam);
    if(typeof this.rounds[match.roundId+1] != "undefined"){
      var nextRound = this.rounds[match.roundId+1],
          nextMatchId = Math.floor(match.matchId/this.teamsPerMatch),
          nextMatch = nextRound.matches[nextMatchId];
      console.log(nextRound,nextMatchId,nextMatch);

      addTeamToMatch(winnerTeam, nextMatch)
    }
    else{
      console.log('Team ' + winnerTeam.teamName + " won the tournament!");
      tournamentWinnerTeamId = winnerTeam.teamId;
      announceWinner(winnerTeam);
    }
  }).bind(this);

  var addTeamToMatch = function (team, match) {
    match.addTeamToMatch(team);
  };

  var initRounds = (function () {
    var numberOfTeamsInNextRound = this.numberOfTeams;

    // var firstRoundMatchups = [];
    //TODO IN ONE ITERATION
    // for(var i =0,length=matchUps.length;i<length;i++){
    //   // firstRoundMatchups[i] = new Match(0, matchUps[i].match, matchUps[i].teamIds);
    //   firstRoundMatchups[i] = new Match(0, matchUps[i].match, []);
    // }
    // this.rounds.push(new Round(0,firstRoundMatchups));

    for(var i=0; i<this.numberOfRounds; i++){
      var currMatchUps = [];

      for(var j=0, numberOfTeamsInNextRound = numberOfTeamsInNextRound/this.teamsPerMatch;j<numberOfTeamsInNextRound;j++){
        //this.rounds[i].matches[this.rounds[i].matches.length-1].matchId + 1 + j
        currMatchUps[j] = new Match(this.tournamentId, i, j, [], this.teamsPerMatch, matchEndCallBack);
      }

      this.rounds.push(new Round(i, currMatchUps));
    }

    console.log(this.rounds);
  }).bind(this);

  var initTeams = (function (matchUps) {
    for(var i=0,length = matchUps.length;i<length;i++){
      for(var j=0,length2=matchUps[i].teamIds.length;j<length2;j++){
        var newTeam = new Team(matchUps[i].teamIds[j]);
        this.teams.push(newTeam);
        (function (i,newTeam,matchUps,addTeamToMatch) {
          newTeam.getTeamInfo(this.tournamentId).then(
            function (response) {
              addTeamToMatch(newTeam, this.rounds[0].getMatchById(matchUps[i].match));
            }.bind(this),
            function (error) {
              console.log(error);
            }
          );
        }).bind(this)(i, newTeam, matchUps, addTeamToMatch);
      }
    }
  }).bind(this);


  var startTournament = (function (tournamentInfo) {
    this.tournamentId = tournamentInfo.tournamentId;

    //Create Rounds
    initRounds(tournamentInfo.matchUps);
    initTeams(tournamentInfo.matchUps);



    // for(var i=0,length=this.rounds[0].matches.length;i<length;i++){
    //
    //   //Get info about teams
    //   for(var k=0;k<this.teamsPerMatch;k++){
    //     var teamId = this.rounds[0].matches[i].teams[k];
    //     this.teams[teamId] = new Team(teamId);
    //     (function(i,teamId){
    //       var match = this.rounds[0].matches[i];
    //       this.teams[teamId].getTeamInfo(this.tournamentId).then(
    //         function (response) {
    //           this.teams[teamId].teamScore = response.score;
    //           this.teams[teamId].teamName = response.name;
    //           this.rounds[0].matches[i].addTeamToMatch(teamId);
    //
    //           if(match.checkIfMatchCanStart(this.teamsPerMatch)){
    //             match.getWinnerTeamId(this.tournamentId, getTeamScoresByMatch(match)).then(
    //               function (response) {
    //                 console.log('winner is: ' + response);
    //               },
    //               function (error) {
    //
    //               }
    //             );
    //           }
    //         }.bind(this),
    //         function(error){
    //           console.log(error);
    //         }
    //       );
    //     }).bind(this)(i,teamId);
    //   }
    //
    //   //Get match scores
    //   (function(i) {
    //     this.rounds[0].matches[i].getMatchScore(this.tournamentId).then(
    //       function (response) {
    //         var match = this.rounds[0].matches[i];
    //         // console.log("matchcanstart", match.checkIfMatchCanStart());
    //         if(match.checkIfMatchCanStart(this.teamsPerMatch)){
    //           match.getWinnerTeamId(this.tournamentId, getTeamScoresByMatch(match)).then(
    //             function (response) {
    //               console.log('winner is: ' + response);
    //             },
    //             function (error) {
    //
    //             }
    //           );
    //         }
    //       }.bind(this),
    //       function (error) {
    //         console.log(error);
    //       }
    //     );
    //   }).bind(this)(i);
    //   // console.log(this.rounds[0].matches[i]);
    // }
    //Start getting info about teams
    //Start getting info about matches

    //If I have scores of teams participating in Match, get winner, and write it to the next round
  }).bind(this);
  var endTournament = function (winnerTeam) {

  };
  getTournamentInfo(teamsPerMatch, numberOfTeams).then(
    function (response) {
      response = JSON.parse(response);
      startTournament(response);
    },
    function (error) {
      console.log(error);
    }
  );


}
(function (window, document) {
  "use strict";
  var windowWidth = document.documentElement.clientWidth,
      windowHeight = document.documentElement.clientHeight,
      tournaments = [],
      tournament,
      raf;

  var onStartClick = function (e) {
    e.preventDefault();
    var teamsPerMatchVal = document.getElementById('teamsPerMatch').value,
      numberOfTeamsVal = document.getElementById('numberOfTeams').value;

    document.getElementById('winner').innerHTML = "";

    if (Helpers.logWithBase(teamsPerMatchVal, numberOfTeamsVal) < 0) {
      document.getElementById('error').innerHTML = "This combination cannot create a tournament";
    }
    else {
      document.getElementById('error').innerHTML = "";
      document.getElementById('start').style.display = "none";
      tournament = new Tournament(teamsPerMatchVal, numberOfTeamsVal);

      createTournamentTemplate(tournament);
    }
    // var tournament = new Tournament(teamsPerMatchVal, numberOfTeamsVal);

    // tournaments.push(tournament);

  };

  var createTournamentTemplate = function (tournament) {
    var oldEl = document.getElementById('tournament-container');
    if(oldEl){
      oldEl.parentNode.removeChild(oldEl);
    }

    var container = document.createElement('div'),
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d"),
        squareSize = 16,
        borderWidth = 1,
        squareMargin = squareSize/2,
        squareColor = "#FFFFFF";

    container.id += "tournament-container";

    container.appendChild(canvas);

    document.body.appendChild(container);

    var render = function render() {
      if(typeof tournament.rounds[0] != "undefined"){
        canvas.width = tournament.rounds[0].matches.length*(squareSize + squareMargin) + squareMargin;
        canvas.height = tournament.numberOfRounds*(squareSize+squareMargin) + squareMargin;
        canvas.style.display = "block";
        for(var i=0,length = tournament.rounds.length;i<length;i++){
          ctx.strokeStyle = squareColor;
          ctx.fillStyle = squareColor;

          for(var j=0,length2=tournament.rounds[i].matches.length;j<length2;j++){
            var currentMatch = tournament.rounds[i].matches[j],
                currMatchY = i*(squareSize+squareMargin) + squareMargin,
                currMatchX = j*(squareSize+squareMargin) + squareMargin;

            // if(i>0){
            //   // var prevStartX = j*tournament.teamsPerMatch*((squareSize+squareMargin) + squareMargin),
            //   //     prevEndX = (j*tournament.teamsPerMatch+tournament.teamsPerMatch-1)*((squareSize+squareMargin) + squareMargin);
            //   //
            //   //     currMatchX = (prevStartX + prevEndX) /2;
            // }


            if(typeof currentMatch.getWinnerTeamId() == "undefined"){
              ctx.rect(currMatchX, currMatchY, squareSize,squareSize);
            }
            else{
              ctx.fillRect(currMatchX, currMatchY, squareSize,squareSize);
            }
          }
        }
        ctx.stroke();
      }

      if(typeof tournament.getWinnerTeamId() == "undefined"){
        requestAnimationFrame(render);
      }
      else{
        cancelAnimationFrame(raf);
        document.getElementById('start').style.display = "block";
      }
    };

    raf = requestAnimationFrame(render);
  };
  var bindEventListeners = function () {
    document.getElementById('start').addEventListener('click', onStartClick);
  };

  var init = function () {
    bindEventListeners();
  };

  document.addEventListener('DOMContentLoaded', function () {
    init();
  });

  window.addEventListener('load', function (e) {

  });

  window.addEventListener('resize', function (e) {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
  });

})(window, document);