var Helpers = (function Helpers(){
  return {
    logWithBase : function(b,n){
      var exp = 0;

      while(true){
        n = n / b;
        exp += 1;

        if(n % 1 != 0){
          return -1;
        }

        if(n == 1) {
          return exp;
        }

      }
    },
    randomColor : function() {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    },
    isTouch : function () {
      return !!('ontouchstart' in window) || window.navigator.msMaxTouchPoints > 0;
    },
    ajax : function (options) {
      if(!options){
        throw "ajaxCall function has no argument - options!";
      }
      else if(!options.url || options.url == ""){
        throw "ajax options has no url specified";
      }
      var customOptions = {
        async : options.async || true,
        data : options.data || {},
        url : options.url,
        method : options.method || 'POST',
        contentType : options.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
        username : options.username || '',
        password : options.password || '',
        error : options.error || function(xhr){console.log(xhr)},
        success : options.success || function(xhr){console.log(xhr)}
      };

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4){
          if(xhttp.status == 200){
            customOptions.success(xhttp.response);
          }
          else{
            customOptions.error(xhttp.response);
          }
        }
      };
      xhttp.open(customOptions.method, customOptions.url, customOptions.async);
      xhttp.setRequestHeader("Content-type", customOptions.contentType);
      (customOptions.method == "GET") ? xhttp.send() : xhttp.send(this.serialize(customOptions.data));
    },
    serialize : function(obj) {
      var str = [];
      for(var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    },
    objToQuery : function (key, arr) {
      var strToReturn = "";
      var counter = 0;
      for(var i in arr){
        if(counter!=0){
          strToReturn += "&";
        }
        strToReturn+= key + "=" + arr[i];
        counter++;
      }
      return strToReturn;
    }
  };
})();