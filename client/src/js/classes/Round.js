function Round(roundId, matchUps){
  this.roundId = roundId;
  this.matches = matchUps;


  this.getMatchById = function (matchId) {
    for(var i=0,length = this.matches.length;i<length;i++){
      if(this.matches[i].matchId == matchId){
        return this.matches[i];
      }
    }
    throw "No matches found with id given";
  }
}