module.exports = function (grunt) {
  // Project configuration.

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: "\n"
      },
      basic_and_extras: {
        files: {
          'client/dist/js/client.js': ['client/src/js/classes/*.js',
                                      'client/src/js/main.js']
        }
      }
    },
    babel: {
      options: {
        sourceMap: true,
        presets: ['babel-preset-es2015']
      },
      dist: {
        files: {
          'client/dist/js/client.js': 'client/src/js/client.js'
        }
      }
    },
    watch: {
      scripts: {
        files: ['client/src/js/*.js','client/src/js/*/*.js'],
        tasks: ['concat']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-babel');
  // grunt.loadNpmTasks('grunt-webpack');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};