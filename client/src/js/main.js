(function (window, document) {
  "use strict";
  var windowWidth = document.documentElement.clientWidth,
      windowHeight = document.documentElement.clientHeight,
      tournaments = [],
      tournament,
      raf;

  var onStartClick = function (e) {
    e.preventDefault();
    var teamsPerMatchVal = document.getElementById('teamsPerMatch').value,
      numberOfTeamsVal = document.getElementById('numberOfTeams').value;

    document.getElementById('winner').innerHTML = "";

    if (Helpers.logWithBase(teamsPerMatchVal, numberOfTeamsVal) < 0) {
      document.getElementById('error').innerHTML = "This combination cannot create a tournament";
    }
    else {
      document.getElementById('error').innerHTML = "";
      document.getElementById('start').style.display = "none";
      tournament = new Tournament(teamsPerMatchVal, numberOfTeamsVal);

      createTournamentTemplate(tournament);
    }
    // var tournament = new Tournament(teamsPerMatchVal, numberOfTeamsVal);

    // tournaments.push(tournament);

  };

  var createTournamentTemplate = function (tournament) {
    var oldEl = document.getElementById('tournament-container');
    if(oldEl){
      oldEl.parentNode.removeChild(oldEl);
    }

    var container = document.createElement('div'),
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d"),
        squareSize = 16,
        borderWidth = 1,
        squareMargin = squareSize/2,
        squareColor = "#FFFFFF";

    container.id += "tournament-container";

    container.appendChild(canvas);

    document.body.appendChild(container);

    var render = function render() {
      if(typeof tournament.rounds[0] != "undefined"){
        canvas.width = tournament.rounds[0].matches.length*(squareSize + squareMargin) + squareMargin;
        canvas.height = tournament.numberOfRounds*(squareSize+squareMargin) + squareMargin;
        canvas.style.display = "block";
        for(var i=0,length = tournament.rounds.length;i<length;i++){
          ctx.strokeStyle = squareColor;
          ctx.fillStyle = squareColor;

          for(var j=0,length2=tournament.rounds[i].matches.length;j<length2;j++){
            var currentMatch = tournament.rounds[i].matches[j],
                currMatchY = i*(squareSize+squareMargin) + squareMargin,
                currMatchX = j*(squareSize+squareMargin) + squareMargin;

            // if(i>0){
            //   // var prevStartX = j*tournament.teamsPerMatch*((squareSize+squareMargin) + squareMargin),
            //   //     prevEndX = (j*tournament.teamsPerMatch+tournament.teamsPerMatch-1)*((squareSize+squareMargin) + squareMargin);
            //   //
            //   //     currMatchX = (prevStartX + prevEndX) /2;
            // }


            if(typeof currentMatch.getWinnerTeamId() == "undefined"){
              ctx.rect(currMatchX, currMatchY, squareSize,squareSize);
            }
            else{
              ctx.fillRect(currMatchX, currMatchY, squareSize,squareSize);
            }
          }
        }
        ctx.stroke();
      }

      if(typeof tournament.getWinnerTeamId() == "undefined"){
        requestAnimationFrame(render);
      }
      else{
        cancelAnimationFrame(raf);
        document.getElementById('start').style.display = "block";
      }
    };

    raf = requestAnimationFrame(render);
  };
  var bindEventListeners = function () {
    document.getElementById('start').addEventListener('click', onStartClick);
  };

  var init = function () {
    bindEventListeners();
  };

  document.addEventListener('DOMContentLoaded', function () {
    init();
  });

  window.addEventListener('load', function (e) {

  });

  window.addEventListener('resize', function (e) {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
  });

})(window, document);