function Match(tournamentId, roundId, matchId, teams, teamsPerMatch, cb){
  this.tournamentId = tournamentId;
  this.roundId = roundId;
  this.matchId = matchId;
  this.teams = teams;
  this.teamsPerMatch = teamsPerMatch;

  var matchScore, winnerTeamId;

  var getMatchScore = function (tournamentId, roundId, matchId) {
    return new Promise(function (resolve, reject) {
      Helpers.ajax({
        method : "GET",
        url : "/match?tournamentId=" +tournamentId+"&round="+roundId+"&match="+matchId,
        success: function(data){
          data = JSON.parse(data);
          matchScore = data.score;
          resolve(matchScore);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };
  var getWinnerScore = function (tournamentId, teamScores, matchScore) {
    return new Promise(function (resolve, reject) {
      Helpers.ajax({
        method : "GET",
        url : "/winner?tournamentId=" + tournamentId + "&" + Helpers.objToQuery("teamScores",teamScores)+"&matchScore="+matchScore,
        success: function(data){
          data = JSON.parse(data);
          resolve(data.score);
        },
        error : function (err) {
          reject(err);
        }
      });
    });
  };

  var getTeamScoresArr = function (teams) {
    var teamScoresArr = [];
    console.log(teams);
    for(var i =0, length=teams.length; i<length;i++){
      teamScoresArr.push(teams[i].teamScore);
    }

    return teamScoresArr;
  };

  var startMatch = (function () {
    getWinnerScore(this.tournamentId, getTeamScoresArr(this.teams), this.matchId).then(
      function (response) {
        var winnerScore = response;
        for(var i = 0, length = this.teams.length; i<length; i++){
          if(winnerScore == this.teams[i].teamScore){
            winnerTeamId=this.teams[i].teamId;
            endMatch();
            break;
          }
        }
      }.bind(this),
      function (error) {
        console.log(error);
      }
    );
  }).bind(this);
  var endMatch = (function () {
    if(typeof winnerTeamId == "undefined") throw "Winner is not defined yet!";
    cb(this, winnerTeamId);
  }).bind(this);
  var checkIfMatchCanStart = (function () {
    if(this.teams.length == this.teamsPerMatch && typeof matchScore != "undefined"){
      startMatch();
    }
  }).bind(this);

  this.addTeamToMatch = function (team) {
    //TODO CHECK IF ARRAY IS ALREADY FULL
    this.teams.push(team);
    checkIfMatchCanStart();
  };
  this.getWinnerTeamId = function () {
    return winnerTeamId;
  };
  getMatchScore(this.tournamentId,this.roundId,this.matchId).then(
    function (response) {
      matchScore = response;
      checkIfMatchCanStart();
    },
    function (error) {
      console.log(error);
    }
  );

}