function Team(teamId){
  this.teamId = teamId;

  var setTeamInfo = (function (teamInfo) {
    this.teamScore = teamInfo.score;
    this.teamName = teamInfo.name;
  }).bind(this);

  var getTeamInfoFromServer = function (tournamentId, teamId, resolve, reject) {
    Helpers.ajax({
      method : "GET",
      url : "/team?tournamentId=" +tournamentId + "&teamId=" + teamId,
      success: function(data){
        var teamInfo = JSON.parse(data);
        setTeamInfo(teamInfo);
        resolve();
      },
      error : function (err) {
        reject(error);
      }
    });
  };

  this.getTeamInfo = function (tournamentId) {
    var teamId = this.teamId;
    return new Promise(function (resolve, reject) {
      if(this.teamScore){
        resolve();
      }
      else{
        getTeamInfoFromServer(tournamentId, teamId, resolve, reject);
      }
    });
  };
}